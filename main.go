package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/himuraxkenji/golang-gopher/gophercises/html-link-parser/link"
)

func main() {

	htmlFile, err := os.ReadFile("html/ex4.html")

	if err != nil {
		log.Fatalf("Error reading html file %v", err)
	}

	exampleHtml := string(htmlFile)

	r := strings.NewReader(exampleHtml)

	links, err := link.Parse(r)

	if err != nil {
		log.Fatalf("Error parsing links %v", err)
	}

	fmt.Printf("%+v\n", links)

}
